import numpy as np
from numpy.random import RandomState

from w2l.neuralnets import ann

from topfarm.constraint_components.load import (
    predict_output,
    predict_gradient)


def test_w2l_1():
    """
    Basic test of predict_output().
    """

    rng = RandomState(0)
    input = -1.0 + 2.0 * rng.rand(100, 1)
    output_ok = 0.1 * input
    gradient_ok = np.full(input.shape, 0.1)

    model = ann(layersizes=[1, 1],
                activation=['Linear', 'Linear'])
    model.train(input, output_ok)
    predicted_output, _ = predict_output(model, input)
    predicted_gradient, _ = predict_gradient(model, input)

    assert np.allclose(output_ok, predicted_output, atol=1e-5)
    assert np.allclose(gradient_ok, predicted_gradient, atol=1e-5)


def test_w2l_2():
    """
    Basic test of predict_output().
    """

    rng = RandomState(0)
    input = -1.0 + 2.0 * rng.rand(100, 2)
    output_ok = (0.1 * input[:, 0] - 0.2 * input[:, 1]).reshape(-1, 1)
    gradient_ok = np.tile([0.1, - 0.2], [input.shape[0], 1])

    model = ann(layersizes=[2, 1],
                activation=['Linear', 'Linear'])
    model.train(input, output_ok)
    predicted_output, _ = predict_output(model, input)
    predicted_gradient, _ = predict_gradient(model, input)

    assert np.allclose(output_ok, predicted_output, atol=1e-4)
    assert np.allclose(gradient_ok, predicted_gradient, atol=1e-5)


def test_w2l_gradient():
    """
    Basic test of predict_gradient.
    """

    input = np.linspace(-2.0, +2.0, 200).reshape(-1, 1)
    output_ok = np.tanh(input)
    exact_gradient = 1.0 - np.tanh(input) ** 2.0

    model = ann(layersizes=[1, 1],
                activation=['Tanh', 'Linear'])
    model.train(input, output_ok)

    predicted_output, _ = predict_output(model, input)
    predicted_gradient, _ = predict_gradient(model, input)

    assert np.allclose(output_ok, predicted_output, atol=0.3)
    assert np.allclose(exact_gradient, predicted_gradient, atol=0.3)
