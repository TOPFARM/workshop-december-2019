from setuptools import setup

setup(
    name='Wind2Loads',
    version='0.0.1',
    description='Load constraint plugin for TopFarm with Wind2Loads for load surrogates',
    install_requires=['topfarm'],
#    package_dir={'': ''},
    dependency_links=['https://gitlab.windenergy.dtu.dk/TOPFARM/TopFarm2.git'],
    packages=['wind2loads', 'w2l'],
    license='All rights reserved',
    entry_points={'topfarm.plugins': 'wind2loads = wind2loads'},
)
