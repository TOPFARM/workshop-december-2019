# %% Import.

import os

import sys
from glob import glob
import pdb, traceback

import numpy as np
import openmdao.api as om
import pickle
from py_wake.examples.data.iea37._iea37 import IEA37Site #,IEA37_WindTurbines
#from py_wake.turbulence_models.stf import NOJ_STF2017, NOJ_STF2005
from py_wake.turbulence_models.stf import STF2017TurbulenceModel, STF2005TurbulenceModel
#from py_wake.wake_models.noj import NOJ
from py_wake.deficit_models.noj import NOJ

from py_wake.aep_calculator import AEPCalculator

import topfarm
from topfarm import TopFarmProblem, TopFarmBaseGroup
from topfarm.easy_drivers import EasyRandomSearchDriver, EasyScipyOptimizeDriver, EasySimpleGADriver
from topfarm.constraint_components.spacing import SpacingConstraint
from topfarm.constraint_components.load import predict_output, predict_gradient, SurrogateModel
from topfarm.plotting import XYPlotComp
from topfarm.drivers.random_search_driver import RandomizeTurbineTypeAndPosition
from topfarm.constraint_components.boundary import XYBoundaryConstraint
from workshop.load_constraint_classes import (WakeComp, LoadComp, LifetimeLoadComp)
from py_wake.examples.data.ParqueFicticio import ParqueFicticio_path
#from py_wake.site import WaspGridSiteBase, UniformWeibullSite
from py_wake.site import WaspGridSite, UniformWeibullSite

#from py_wake.examples.data.DTU10MW_RWT import DTU10MW
from py_wake.examples.data.dtu10mw import DTU10MW

from py_wake.site.shear import PowerShear
import workshop
import copy
"""
Meaning of the indices in a_i1i2lk:
- i1 upstream turbine
- i2 downstream turbine
- l wind direction
- k wind speed
"""
topfarm.type_key = 'turbineType'

# %% Load the surrogates made with wind2loads.
path = workshop.w2l_path
file_list = [x[2] for x in os.walk(path)][0]
#file_list.pop(0)
#load_types = dict.fromkeys([os.path.basename(n) for n in file_list])
load_types = {}
for file in file_list:
    with open(os.path.join(path, file), 'rb') as f:
#        model = 
        load_types[file[:-4]] = pickle.load(f)
#for f, l in zip(file_list, load_types):
#    model = tf.keras.models.load_model(os.path.join(f, 'model.h5'))
#    with open(os.path.join(f, 'save_dic.pkl'), 'rb') as g:
#        save_dic = pickle.load(g)
#    load_types[l] = SurrogateModel(model, **save_dic)
#wind2power = load_types['P']
[load_types.pop(n) for n in ['Power']]
for key in load_types:
    load_types[key].input_channel_names = ['wsp', 'tint', 'shear', 'MannL', 'MannGamma', 'Veer', 'Yaw', 'tein', 'air_den']
#    load_type['tint'] = load_type['SigmaU']/load_type['U']
#sys.path.append(r'..\..\..\wf_load_surrogate\src\Two_Turbines_only_Wind')
#from input_boundary import boundary

def _add_wind_shear(site, alpha_far=0.143):
    ds = site._ds
    ds['wind_shear'] = copy.deepcopy(ds['spd'])

    heights = ds['wind_shear'].coords['z'].data

    # if there is only one layer, assign default value
    if len(heights) == 1:

        ds['wind_shear'].data = (np.zeros_like(ds['wind_shear'].data) +
                                      alpha_far)

        print('Note there is only one layer of wind resource data, '
              + 'wind shear are assumed as uniform, i.e., {0}'.format(
                      alpha_far))
    else:
        ds['wind_shear'].data[:, :, 0, :] = (alpha_far +
         np.log(ds['spd'].data[:, :, 0, :]/ds['spd'].data[:, :, 1, :])/
         np.log(heights[0]/heights[1]))

        for h in range(1, len(heights)):
            ds['wind_shear'].data[:, :, h, :] = (alpha_far +
         np.log(ds['spd'].data[:, :, h, :]/ds['spd'].data[:, :, h-1, :])/
         np.log(heights[h]/heights[h-1]))

# %% Set up the optimization problem.

# Add the wake model to the site.
n_wt = 9
if 0:
    #site = WaspGridSiteBase.from_wasp_grd(ParqueFicticio_path, speedup_using_pickle=False)
    site = WaspGridSite.from_wasp_grd(ParqueFicticio_path, speedup_using_pickle=False)
    site.interp_funcs_initialization(['wind_shear'])
    site.default_ws = np.linspace(10,12,1)
    if 'wind_shear' not in site._ds.data_vars:
        _add_wind_shear(site)
    x, y = site._ds.coords['x'].data, site._ds.coords['y'].data,
    Y, X = np.meshgrid(y, x)
    Z = site._ds['elev'].data
    index = Z>0
    x_min = min(X[index])
    x_max = max(X[index])
    y_min = min(Y[index])
    y_max = max(Y[index])
    initial_positions = np.array([[ 263912,  263189,  264253,  263194,  263442,  264535],
           [6505083, 6505989, 6505614, 6505394, 6506340, 6505316]]).T
    boundary = np.array([(x_min, y_min), (x_max, y_min), (x_max, y_max), (x_min, y_max)])
else:
    f = [0.035972, 0.039487, 0.051674, 0.070002, 0.083645, 0.064348,
             0.086432, 0.117705, 0.151576, 0.147379, 0.10012, 0.05166]
    A = [9.176929, 9.782334, 9.531809, 9.909545, 10.04269, 9.593921,
         9.584007, 10.51499, 11.39895, 11.68746, 11.63732, 10.08803]
    k = [2.392578, 2.447266, 2.412109, 2.591797, 2.755859, 2.595703,
         2.583984, 2.548828, 2.470703, 2.607422, 2.626953, 2.326172]
    ti = 0.001
    h_ref = 100
    alpha = .1
    site = UniformWeibullSite(f, A, k, ti, shear=PowerShear(h_ref=h_ref, alpha=alpha))

#    site = IEA37Site(64)
    if 0:
        x_min = 0
        x_max = 5000
        y_min = 0
        y_max = 5000
        x,y= np.meshgrid(np.linspace(500,4500,3),
               np.linspace(500,4500,3))
        initial_positions = np.column_stack((x.ravel(),y.ravel()))
        boundary = np.array([(x_min, y_min), (x_max, y_min), (x_max, y_max), (x_min, y_max)])
    if 1:
#        x_min = 0
#        x_max = 7*200*5
#        y_min = x_min
#        y_max = x_max
        spacing = 2000
        N = 5
        theta = 76 # deg
        dx = np.tan(np.radians(theta))
        x = np.array([np.linspace(0,(N-1)*spacing,N)+i*spacing/dx for i in range(N)])
        y = np.array(np.array([N*[i*spacing] for i in range(N)]))
        initial_positions = np.column_stack((x.ravel(),y.ravel()))
        eps = 2000
        boundary = np.array([(0, 0),
                             ((N-1)*spacing+eps, 0),
                             ((N-1)*spacing*(1+1/dx)+eps*(1+np.cos(np.radians(theta))), (N-1)*spacing+eps*np.sin(np.radians(theta))),
                             ((N-1)*spacing/dx+eps*np.cos(np.radians(theta)), (N-1)*spacing+eps*np.sin(np.radians(theta)))])
#    sit

windTurbines = DTU10MW()
#wake_model = NOJ_STF2017(site, windTurbines)
#wake_model = NOJ_STF2005(site, windTurbines)
wake_model = NOJ(site, windTurbines)
aep_calculator = AEPCalculator(wake_model)


#initial_pos = np.array([np.random.randint(min(X[index]), max(X[index]), n_wt),
#                        np.random.randint(min(Y[index]), max(Y[index]), n_wt)])
# Turbines initial positions.
site.initial_position = initial_positions
# Number of turbines.
n_wt = len(initial_positions)

# Minimum distance.
min_spacing = 3 * windTurbines.diameter(0)

# Number of wind directions.
n_wd = 360

# Number of wind speeds.
n_ws = 23

    
# Turbine types
turb_types = ['DTU10MW']

# Number of types.
n_types = len(turb_types)

# Load signals to constrain
load_signals = ['Blade_root_edgewise_M_y',
                'Blade_root_flapwise_M_x',
                'Tower_base_fore_aft_M_x',
                'Tower_base_side_side_M_y']

# Maximum load for each load channel and type.
max_load = {'Blade_root_edgewise_M_y': np.array([34000]),
            'Blade_root_flapwise_M_x': np.array([34000]),
            'Tower_base_fore_aft_M_x': np.array([34000]),
            'Tower_base_side_side_M_y': np.array([34000]),
            }
load_compute_method='Frandsen'

# Store the load surrogates in a dictionary with two levels for Frandsen.
# level 1: load channel
# level 2: turbine type

models = {}
for ls in load_signals:
    models[ls] = {}
    for upstream in turb_types:
        models[ls][upstream] = load_types[ls]

# Site constant parameters.

# Air density [kg/m3].
air_den = 1.225

# Wind shear exponent [-].
shear = 0.3

# Terrain inclination.
tein = 0.0

# Maximum turbines distance in the surrogates.
wt_dist_max = 20.0

# Maximum wind direction in the surrogates.
wdir_max = 20.0

# Wind direction to use for the unwaked case. Must lie in +/- wdir_max.
wdir_unwaked = 10.0

# Wohler exponent for each load channel.
m_all = {
        'Blade_root_edgewise_M_y': 10.0,
        'Blade_root_flapwise_M_x': 10.0,
        'Tower_base_fore_aft_M_x': 4.0,
        'Tower_base_side_side_M_y': 4.0,
}

# Parameters for the lifetime DEL.
lifetime = float(60 * 60 * 24 * 365 * 20)
f1zh = 10.0 ** 7.0
lifetime_on_f1zh = lifetime / f1zh

#cut_in_out = []
#wstemp = np.arange(0, 50, 0.1)
#for i, powercurve in enumerate(windTurbines.power_funcs):
#    oi = np.diff(powercurve(wstemp))
#    cutout = wstemp[np.argmin(oi)]
#    cutin = wstemp[np.nonzero(oi)[0][0]]
#    cut_in_out.append([cutin, cutout])
#cut_in_out = np.array(cut_in_out)
#
## TODO check actual idling load values
## for now it is set to 1; no contribution to the DEQL below/above cut in/out.
#idling_loads = {ch: np.ones((n_types, 2)) for ch in load_signals}
# %% TopFarm2 optimization problem.

wake_comp = WakeComp(n_wt=n_wt, n_ws=n_ws, n_wd=n_wd,
                     power_compute_method='PyWake',   # 'PyWake'
                     aep_calculator=aep_calculator,
                     turbineType=np.zeros(n_wt),
                     load_compute_method=load_compute_method,
                     m_all=m_all,
                     objective=True,
                     )

load_comp = LoadComp(n_wt=n_wt, n_ws=n_ws, n_wd=n_wd,
                     models=models, shear=shear, n_types=n_types,
                     air_den=air_den, tein=tein,
                     wdir_unwaked=wdir_unwaked, wdir_max=wdir_max,
                     wt_dist_max=wt_dist_max,
                     load_signals=load_signals, turb_types=turb_types,
                     turbineType=np.zeros(n_wt),
                     load_compute_method=load_compute_method,
#                     cut_in_out=cut_in_out,
#                     idling_loads=idling_loads,
                     m_all=m_all,
                     skip_partials=True,
#                     MannL, 'MannGamma', 'Veer', 'Yaw',
                     )

lifetime_comp = LifetimeLoadComp(n_wt=n_wt, n_ws=n_ws, n_wd=n_wd,
                                 lifetime_on_f1zh=lifetime_on_f1zh,
                                 m_all=m_all,
                                 max_load=max_load, load_signals=load_signals,
                                 turbineType=np.zeros(n_wt),
                                 load_compute_method=load_compute_method,
                     skip_partials=True,
                                 )

fd_group = om.Group()
fd_group.add_subsystem('wake_comp', wake_comp,promotes=['*'])
analytical_group = TopFarmBaseGroup([load_comp, lifetime_comp], output_key='cost')
analytical_group.add_subsystem('load_comp', load_comp, promotes=['*'])
analytical_group.add_subsystem('lifetime_comp', lifetime_comp, promotes=['*'])

class ApproxTotalsGroup(om.Group):
    def __init__(self, groups, inputs):
        super().__init__()
        self.approx_totals_groups = groups
        self.approx_totals_inputs = inputs

    def setup(self):
        super().setup()
        subjac_keys = []
        for group in self.approx_totals_groups:
            self.add_subsystem(group[0], group[1], promotes=['*'])
            for out in group[2]:
                for in_group in self.approx_totals_inputs:
                    for inp in in_group[2]:
                        subjac_keys.append(('cost_comp.{}.{}'.format(group[0], out), 'cost_comp.{}.{}'.format(in_group[0], inp)))
#        print(subjac_keys)
        self.approx_totals()
        self._approx_subjac_keys = subjac_keys
cost_group = ApproxTotalsGroup(groups=[('fd_group', fd_group, ['wake_comp.cost']),
                                       ('analytical_group', analytical_group, ['lifetime_comp.' + ls for ls in load_signals]),#, 'lifetime_comp.MxB2']),
                                       ],
                                inputs=[('fd_group', fd_group, ['wake_comp.x', 'wake_comp.y'])])
def get_load_cost_comp():
    return cost_group

if __name__ == '__main__':
    try:
        xy_problem = TopFarmProblem(
                design_vars={topfarm.x_key: site.initial_position[:, 0],
                             topfarm.y_key: site.initial_position[:, 1]},
                cost_comp=get_load_cost_comp(),
        #            driver=EasyRandomSearchDriver(randomize_func=RandomizeTurbinePosition_Circle(max_step=2000), max_iter=50),
                driver=EasyScipyOptimizeDriver(maxiter=100),
        #            driver=EasySimpleGADriver(),
                constraints=[SpacingConstraint(min_spacing),
                             XYBoundaryConstraint(boundary),],
                    post_constraints=[(ls, 1.0) for ls in load_signals],
                plot_comp=XYPlotComp(),
                approx_totals={'step':10},
                expected_cost=1e-6,
                )
    
        cost, state = xy_problem.evaluate()
        max_load = {ls: np.array([xy_problem[ls+'_abs'].max()]) for ls in load_signals}
        xy_problem.cost_comp.analytical_group.lifetime_comp.options['max_load'] = max_load
    #    # Run the optimization.
        cost, state, recorder = xy_problem.optimize(disp=True)
        recorder.save()
    
    except Exception:
        extype, value, tb = sys.exc_info()
        print(traceback.print_exc())
        pdb.post_mortem(tb)
    
    # Test gradient.
    if False:
        with open('./check_partials_xy.txt', 'w') as fid:
            partials = xy_problem.check_partials(out_stream=fid,
                                                 compact_print=True,
                                                 show_only_incorrect=True,
                                                 step=0.01)
    
    # Test model structure.
    #om.view_model(xy_problem)
    #%%
    #xy_problem = TopFarmProblem(
    #        design_vars={topfarm.x_key: site.initial_position[:, 0],
    #                     topfarm.y_key: site.initial_position[:, 1]},
    #        cost_comp=cost_group,
    ##            driver=EasyRandomSearchDriver(randomize_func=RandomizeTurbinePosition_Circle(max_step=2000), max_iter=50),
    #        driver=EasyScipyOptimizeDriver(maxiter=100, tol=1e-8),
    ##            driver=EasySimpleGADriver(),
    #        constraints=[SpacingConstraint(min_spacing),
    #                     XYBoundaryConstraint(boundary),],
    #            post_constraints=[(ls, 1.0) for ls in load_signals],
    #        plot_comp=XYPlotComp(),
    #        approx_totals={'step':5},
    #        expected_cost=1.0,
    #        )
    #cost2, state2, recorder2 = xy_problem.optimize(state=state, disp=True)

