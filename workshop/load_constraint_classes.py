# %% Define the classes needed to apply the load constraint.

# %% Import.

import numpy as np

import openmdao.api as om

import topfarm
from topfarm.constraint_components.load import (
        predict_output, predict_gradient)
from py_wake.aep_calculator import AEPCalculator

def _as_input_or_option(inputs, options, key):
    if key in inputs:
        return inputs[key]
    elif key in options:
        return options[key]
    else:
        raise ValueError('{} needs to be either an input or an option for this component'.format(key))

# %% Classes definition.

class WakeComp(om.ExplicitComponent):
    """
    Component that receives the turbines positions and computes the wake.
    """
    WtoGWh = 24.0 * 365.0 * 1e-9
    cost_factor = -1.0

    def initialize(self):
        self.options.declare('n_wt', types=int, desc='number of wind turbines')
        self.options.declare('n_wd', types=int, desc='number of wind directions')
        self.options.declare('n_ws', types=int, desc='number of wind speeds')
        self.options.declare('aep_calculator', types=AEPCalculator, desc='AEP calculator')
        self.options.declare('power_compute_method', default='PyWake', types=str, desc='method to compute the power')
        self.options.declare('load_compute_method', default=None, types=(str, type(None)), desc='method to compute the loads. Frandsen: uses the aggregated TI approach (sums all wind directions: Annex E IEC 61400-1:2019)')
        self.options.declare(topfarm.type_key, default=None, types=(np.ndarray, type(None)), desc='turbine type as an option')
        self.options.declare('skip_partials', default=True, types=bool, desc='if this component is part of a group where approx totals is utilized or gradient free optimization is used then the partials should not be declared')
        self.options.declare('objective', default=False, types=bool, desc='True if this component provides the objective function')
        self.options.declare('m_all', default={}, types=dict, desc='dictionary of Wohler exponent for each load channel')
        self.options.declare('additional_output', default={}, types=dict, desc='dict where keys are output name and value value shape of additional output')

    def setup(self):
        # Options.
        self.shape_i = (self.options['n_wt'])
        self.shape_ik = (self.options['n_wt'],
                          self.options['n_ws'])
        self.shape_il = (self.options['n_wt'],
                          self.options['n_wd'])
        self.shape_ilk = (self.options['n_wt'],
                          self.options['n_wd'],
                          self.options['n_ws'])
        self.shape_ikm = (self.options['n_wt'],
                          self.options['n_ws'],
                          len(list(set(self.options['m_all'].values()))))
        self.shape_iilk = (self.options['n_wt'],
                           self.options['n_wt'],
                           self.options['n_wd'],
                           self.options['n_ws'])
        if self.options['power_compute_method'] not in ('PyWake', 'wind2power'):
            raise ValueError('power_compute_method can only be PyWake or wind2power.')
        # Input.
        self.add_input(topfarm.x_key, shape=self.options['n_wt'])
        self.add_input(topfarm.y_key, shape=self.options['n_wt'])
        if self.options[topfarm.type_key] is None: # add turbine type as an input if it is not an option
            self.add_input(topfarm.type_key, shape=self.options['n_wt'])

        # Output shapes
        output_lists = ['outputs', 'outputs_i', 'outputs_ik', 'outputs_ikm', 'outputs_ilk', 'outputs_iilk']
        shapes = [1, self.shape_i, self.shape_ik, self.shape_ikm, self.shape_ilk, self.shape_iilk]
        o_dict = {i: [] for i in output_lists}

        # Power compute method and objective
        if self.options['power_compute_method'] == 'PyWake':
            if self.options['objective']:
                o_dict['outputs'].extend(['cost', 'Cost'])
            else:
                o_dict['outputs'].append('AEP')
                o_dict['outputs_i'].append('AEP_i')
        elif self.options['power_compute_method'] == 'wind2power' or self.options['load_compute_method'] == 'DWM':
            o_dict['outputs_ilk'].extend(['WS_eff_ilk', 'TI_eff_ilk', 'pdf_ilk'])
        else:
            raise ValueError('power_compute_method can only be PyWake or wind2power.')

        # Load compute method
        if self.options['load_compute_method'] == 'Frandsen':
            o_dict['outputs_ikm'].append('TI_eff_ikm')
            o_dict['outputs_ik'].append('WS_eff_ik')
            o_dict['outputs_ik'].append('pdf_ik')
        elif self.options['load_compute_method'] == 'DWM':
            o_dict['outputs_iilk'].extend(['wdir_iilk', 'wt_dist_iilk'])
        else:
            print('No load calculation is specified and wake component will not output wake results')

        # Add output and gradients
        for list_name, shape in zip(output_lists, shapes):
            output_list = o_dict[list_name]
            for output in output_list:
                self.add_output(output, shape=shape)
            if not self.options['skip_partials']:
                self.declare_partials(of=output_list, wrt=[topfarm.x_key, topfarm.y_key], method='fd')
        for k, v in self.options['additional_output'].items():
            if k == 'wind_shear':
                if self.options['load_compute_method'] == 'Frandsen':
                    self.add_output('wind_shear_ik', shape=self.shape_ik)
                if self.options['power_compute_method'] == 'wind2power':
                    self.add_output('wind_shear_il', shape=self.shape_il)
            else:
                self.add_output(k, shape=v)
            if not self.options['skip_partials']:
                self.declare_partials(of=k, wrt=[topfarm.x_key, topfarm.y_key], method='fd')
        
    def compute(self, inputs, outputs):

        x = inputs[topfarm.x_key]
        y = inputs[topfarm.y_key]
        types = np.round(np.asarray(_as_input_or_option(inputs, self.options, topfarm.type_key))).astype(np.int)
        aep_calculator = self.options['aep_calculator']
        AEP_GWh_ilk = aep_calculator.calculate_AEP(x_i=x, y_i=y, type_i=types)
        dw_iil, hcw_iil, _, _ = aep_calculator.site.wt2wt_distances(
                x_i=x, y_i=y, h_i=aep_calculator.windTurbines.hub_height(types),
                wd_il=aep_calculator.WD_ilk.mean(2))
        pdf_ilk = np.broadcast_to(aep_calculator.P_ilk, self.shape_ilk)
        WS_eff_ilk = aep_calculator.WS_eff_ilk
        TI_eff_ilk = aep_calculator.TI_eff_ilk
        # Power compute method and objective
        if self.options['power_compute_method'] == 'PyWake':
            if self.options['objective']:
                outputs['cost'] = AEP_GWh_ilk.sum() * self.cost_factor
                outputs['Cost'] =  AEP_GWh_ilk.sum()
            else:
                outputs['AEP'] = AEP_GWh_ilk.sum()
                outputs['AEP_i'] = AEP_GWh_ilk.sum((1, 2))
        elif self.options['power_compute_method'] == 'wind2power' or self.options['load_compute_method'] == 'DWM':
            outputs['WS_eff_ilk'] = WS_eff_ilk
            outputs['TI_eff_ilk'] = TI_eff_ilk
            outputs['pdf_ilk'] = pdf_ilk
            
        # Load compute method
        if self.options['load_compute_method'] == 'Frandsen':
            m_list = list(set(self.options['m_all'].values()))
            for cou, m in enumerate(m_list):
                outputs['TI_eff_ikm'][:, :, cou] = ((pdf_ilk * TI_eff_ilk ** m).sum(1)) ** (1 / m) / pdf_ilk.sum(1)       
            outputs['WS_eff_ik'] = (pdf_ilk * WS_eff_ilk).sum(1) / pdf_ilk.sum(1)
            outputs['pdf_ik'] = pdf_ilk.sum(1)
        elif self.options['load_compute_method'] == 'DWM':
            wdir_iil = np.rad2deg(np.arctan2(hcw_iil, dw_iil))
            # wdir_iil[i, i, :] can either be 0 or 180. We set it to 180 to
            # properly detect the unwaked cases.
            for i in range(self.options['n_wt']):
                wdir_iil[i, i, :] = 180.0
    
            # Compute turbines distances.
            # TODO: make it type-dependent
            wt_dist_iil = np.hypot(dw_iil, hcw_iil) / aep_calculator.windTurbines.diameter()
    
            # Replicate wind directions over the wind speeds.
            wdir_iilk = np.broadcast_to(wdir_iil[:, :, :, np.newaxis], self.shape_iilk)
        
            # Replicate turbines distances over the wind speeds.
            wt_dist_iilk = np.broadcast_to(wt_dist_iil[:, :, :, np.newaxis], self.shape_iilk)
            outputs['wdir_iilk'] = wdir_iilk
            outputs['wt_dist_iilk'] = wt_dist_iilk
        else:
            print('No load calculation is specified and wake component will not output wake results')
        for k, v in self.options['additional_output'].items():            
            if any([True for iii in outputs if k in iii]) and k in aep_calculator.site.interp_funcs:
                value_il, = aep_calculator.site.interpolate(keys=[k], x_i=x, y_i=y, h_i=aep_calculator.windTurbines.hub_height(types))
                if k == 'wind_shear':
                    if self.options['load_compute_method'] == 'Frandsen':
                        shear_ilk = np.broadcast_to(value_il[:,:,np.newaxis], self.shape_ilk)
                        outputs['wind_shear_ik'] = (pdf_ilk * shear_ilk).sum(1) / pdf_ilk.sum(1)
                    if self.options['power_compute_method'] == 'wind2power':
                       outputs['wind_shear_il'] = value_il
                else:
                    outputs[k] = value_il


class PowerComp(om.ExplicitComponent):
    """
    Component that receives the flow condition and compute the power of all
    turbines.
    """

    def initialize(self):
        #
        self.options.declare('n_wt', types=int, desc='number of wind turbines')
        self.options.declare('n_wd', types=int, desc='number of wind direction cases')
        self.options.declare('n_ws', types=int, desc='number of wind speed cases')
        self.options.declare('n_types', types=int, desc='number of wind turbine types')
        self.options.declare('power_models', types=dict, desc='dictionary of power surrogates for all turbine types')
        self.options.declare('shear', types=(tuple, float, str, int), default=0,
                             desc='tuple of wind shear type and value i.e. ("ambient", [alpha]) or ("PyWake", ), alpha as float, or method ("PyWake" or "ambient") as string')
        self.options.declare('air_den', types=float, desc='air density')
        self.options.declare('wt_dist_max', types=float, desc='maximum distance in wind2power')
        self.options.declare('tein', types=float, desc='terrain inclination')
        self.options.declare('wdir_unwaked', types=float, desc='wind direction for unwaked conditions')
        self.options.declare('turb_types', types=list, desc='list of turbine types names')
        self.options.declare(topfarm.type_key, default=None, types=(np.ndarray, type(None)), desc='turbine type as an option')
        self.options.declare('skip_partials', default=False, types=bool, desc='if this component is part of a group where approx totals is utilized or gradient free optimization is used then the partials should not be declared')
        self.options.declare('cut_in_out', default=None, types=(np.ndarray, type(None)), desc='numpy array composed of multiple lists for each turbine cut in/out i.e. np.array([3,20],[4,25],[5,25])')
        self.options.declare('MannL', default=29.4)
        self.options.declare('MannGamma', default=3.9)
        self.options.declare('Veer', default=0)
        self.options.declare('Yaw', default=0)

    def setup(self):
        self.shape_ilk = (self.options['n_wt'],
                          self.options['n_wd'],
                          self.options['n_ws'])
        self.shape_il = (self.options['n_wt'],
                          self.options['n_wd'])
        self.size_ilk = np.prod(self.shape_ilk)
        shear = self.options['shear']
        if isinstance(shear, tuple):
            self.shear_method, self.alpha = shear
        elif isinstance(shear, (float, int)):
            self.shear_method, self.alpha = ('ambient', shear)
        elif isinstance(shear, str):
            self.shear_method, self.alpha = (shear, np.nan)
        else:
            raise ValueError('shear should be a tuple of shear method and alpha, a float of alpha or a string of shear method')
        if self.shear_method == 'PyWake':
            self.add_input('wind_shear_il', shape=self.shape_il)
        elif self.shear_method == 'ambient':
            self.shear_il = np.broadcast_to(self.alpha, self.shape_il)
        self.air_den_ilk = np.broadcast_to(self.options['air_den'], self.shape_ilk)
        self.wt_dist_max_ilk = np.broadcast_to(self.options['wt_dist_max'], self.shape_ilk)
        self.tein_ilk = np.broadcast_to(self.options['tein'], self.shape_ilk)
        self.wdir_unwaked_ilk = np.broadcast_to(self.options['wdir_unwaked'], self.shape_ilk)

        # Output.
        self.add_output('power_ilk', shape=self.shape_ilk)

        # Because of src_indices, these inputs are _ilk.
        self.add_input('WS_eff_ilk', shape=self.shape_ilk)
        self.add_input('TI_eff_ilk', shape=self.shape_ilk)
        self.MannL_ilk = np.broadcast_to(self.options['MannL'], self.shape_ilk)
        self.MannGamma_ilk = np.broadcast_to(self.options['MannGamma'], self.shape_ilk)
        self.Veer_ilk = np.broadcast_to(self.options['Veer'], self.shape_ilk)
        self.Yaw_ilk = np.broadcast_to(self.options['Yaw'], self.shape_ilk)
        if self.options[topfarm.type_key] is None: # add turbine type as an input if it is not an option
            self.add_input(topfarm.type_key, shape=self.options['n_wt'])
        # In this case the Jacobian is a diagonal matrix, sparse declaration.
        if not self.options['skip_partials']:
            self.declare_partials('power_ilk', ['WS_eff_ilk', 'TI_eff_ilk'],
                                  method='exact',
                                  rows=np.arange(self.size_ilk),
                                  cols=np.arange(self.size_ilk))
            if self.shear_method == 'PyWake':
                self.declare_partials('power_ilk', 'wind_shear_il', method='exact',
                                      cols=np.arange(self.options['n_wt']*self.options['n_wd']).repeat(self.options['n_ws']),
                                      rows=np.arange(self.size_ilk))
#        assert self.options['cut_in_out'].shape!=(), "!Error! cut_in_out can not be none, PowerComp"            

    def compute(self, inputs, outputs):
        types = np.round(np.asarray(_as_input_or_option(inputs, self.options, topfarm.type_key))).astype(np.int)
        if self.shear_method == 'PyWake':
            self.shear_il = inputs['wind_shear_il'].reshape(self.shape_il)
        shear_ilk = np.broadcast_to(self.shear_il[:,:,np.newaxis], self.shape_ilk)
        # The surrogate is evaluated in the unwaked configuration, using
        # the effective wind speed and turbulence intensity.
        power_ilk = np.zeros(self.shape_ilk)
        for tt, turbine in enumerate(self.options['turb_types']):
            type_index = np.full(self.shape_ilk, True)
            type_index[types != tt, :, :] = False  # TODO: find a oneliner for this
            if np.array(type_index).any():
                wind2power = self.options['power_models'][turbine]
                dummy, _ = predict_output(
                        model=wind2power.model,
                        input={
                            'tint': inputs['TI_eff_ilk'][type_index].ravel(),
                            'shear': shear_ilk[type_index].ravel(),
                            'wsp': inputs['WS_eff_ilk'][type_index].ravel(),
                            'air_den': self.air_den_ilk[type_index].ravel(),
                            'wt_dist': self.wt_dist_max_ilk[type_index].ravel(),
                            'tein': self.tein_ilk[type_index].ravel(),
                            'wdir': self.wdir_unwaked_ilk[type_index].ravel(),
                            'MannL': self.MannL_ilk[type_index].ravel(),
                            'MannGamma': self.MannGamma[type_index].ravel(),
                            'Veer': self.Veer[type_index].ravel(),
                            'Yaw': self.Yaw[type_index].ravel(),
                            },
                        model_in_keys=wind2power.input_channel_names,
                        input_scaler=wind2power.input_scaler,
                        output_scaler=wind2power.output_scaler
                        )  # TODO: boundary=boundary
                power_ilk[type_index] = dummy.ravel()
        power_ilk = power_ilk.reshape(self.shape_ilk)
        # Return the output.
        if self.options['cut_in_out'] is not None:
            idx = np.logical_or(inputs['WS_eff_ilk'] < self.options['cut_in_out'][types][:, 0][:, np.newaxis, np.newaxis],
                                inputs['WS_eff_ilk'] > self.options['cut_in_out'][types][:, 1][:, np.newaxis, np.newaxis])
            power_ilk[idx] = 0.0
        outputs['power_ilk'] = power_ilk

    def compute_partials(self, inputs, partials):
#        if self.options['power_compute_method'] == 'wind2power':
        if self.shear_method == 'PyWake':
            self.shear_il = inputs['wind_shear_il'].reshape(self.shape_il)
        shear_ilk = np.broadcast_to(self.shear_il[:,:,np.newaxis], self.shape_ilk)
        wind2power = self.options['power_models'][self.options['turb_types'][0]]

        WS_eff_ilk = inputs['WS_eff_ilk'].reshape(self.shape_ilk)
        TI_eff_ilk = inputs['TI_eff_ilk'].reshape(self.shape_ilk)
        types = np.round(np.asarray(_as_input_or_option(inputs, self.options, topfarm.type_key))).astype(np.int)

        # The surrogate is evaluated in the unwaked configuration, using
        # the effective wind speed and turbulence intensity.
        Dpowers = {}
        for channel in wind2power.input_channel_names:
            Dpowers[channel] = np.zeros(self.shape_ilk)
        for tt, turbine in enumerate(self.options['turb_types']):
            type_index = np.full(self.shape_ilk, True)
            type_index[types != tt, :, :] = False  # TODO: find a oneliner for this
            if np.array(type_index).any():
                wind2power = self.options['power_models'][turbine]
                dummy, _ = predict_gradient(
                        model=wind2power.model,
                        input={
                            'tint': TI_eff_ilk[type_index].ravel(),
                            'shear': shear_ilk[type_index].ravel(),
                            'wsp': WS_eff_ilk[type_index].ravel(),
                            'air_den': self.air_den_ilk[type_index].ravel(),
                            'wt_dist': self.wt_dist_max_ilk[type_index].ravel(),
                            'tein': self.tein_ilk[type_index].ravel(),
                            'wdir': self.wdir_unwaked_ilk[type_index].ravel(),
                            'MannL': self.MannL_ilk[type_index].ravel(),
                            'MannGamma': self.MannGamma_ilk[type_index].ravel(),
                            'Veer': self.Veer_ilk[type_index].ravel(),
                            'Yaw': self.Yaw_ilk[type_index].ravel(),
                            },
                        model_in_keys=wind2power.input_channel_names,
                        input_scaler=wind2power.input_scaler,
                        output_scaler=wind2power.output_scaler,
                        )  # TODO: boundary=boundary
                for n, channel in enumerate(wind2power.input_channel_names):
                    Dpowers[channel][type_index] = dummy[:,n].ravel()
        if self.options['cut_in_out'] is not None:
            idx = np.logical_or(inputs['WS_eff_ilk'] < self.options['cut_in_out'][types][:, 0][:, np.newaxis, np.newaxis],
                                inputs['WS_eff_ilk'] > self.options['cut_in_out'][types][:, 1][:, np.newaxis, np.newaxis])
            for vals in Dpowers.values():
                vals[idx] = 0.0
                    
        partials['power_ilk', 'WS_eff_ilk'] = Dpowers['wsp'].ravel()
        partials['power_ilk', 'TI_eff_ilk'] = Dpowers['tint'].ravel()
        if self.shear_method == 'PyWake':
            partials['power_ilk', 'wind_shear_il'] = Dpowers['shear'].ravel()


class AEPComp(om.ExplicitComponent):
    """
    Component that receives the power of all turbines and computes the AEP.
    """

    output_key = 'cost'   # AEP
    output_unit = 'GWh'
#    income_model = True
    cost_factor = -1.0

    # Factor to convert W in GWh.
#    WtoGWh = 24.0 * 365.0 * 1e-9
    MWtoGWh = 24.0 * 365.0 * 1e-3
    kWtoGWh = 24.0 * 365.0 * 1e-6

    def initialize(self):
        self.options.declare('n_wt', types=int, desc='number of wind turbines')
        self.options.declare('n_wd', types=int, desc='number of wind direction cases')
        self.options.declare('n_ws', types=int, desc='number of wind speed cases')
        self.options.declare('objective', types=bool, desc='True if this component provides the objective function')
        self.options.declare('skip_partials', default=False, types=bool, desc='if this component is part of a group where approx totals is utilized or gradient free optimization is used then the partials should not be declared')

    def setup(self):
        # Options.
        shape_ilk = (self.options['n_wt'],
                     self.options['n_wd'],
                     self.options['n_ws'])
        size_ilk = np.prod(shape_ilk)
        # Input.
        self.add_input('pdf_ilk', shape=shape_ilk, desc='Probability')
        self.add_input('power_ilk', shape=shape_ilk, desc='Power [MW]')
        # Output.
        if self.options['objective']:
            self.add_output('cost', shape=1, desc='-AEP')
            self.add_output('Cost', shape=1, desc='AEP')
            if not self.options['skip_partials']:
                self.declare_partials('cost', ['pdf_ilk', 'power_ilk'], method='exact',
                                      cols = np.arange(size_ilk),
                                      rows = np.zeros(size_ilk))
                self.declare_partials('Cost', ['pdf_ilk', 'power_ilk'], method='exact',
                                      cols = np.arange(size_ilk),
                                      rows = np.zeros(size_ilk))
        else:  # self.options['objective'] == False
            self.add_output('AEP', shape=1, desc='AEP')
            self.add_output('AEP_i', shape=self.options['n_wt'], desc='AEP for each turbine')
            if not self.options['skip_partials']:
                self.declare_partials('AEP', ['pdf_ilk', 'power_ilk'], method='exact',
                                      cols = np.arange(size_ilk),
                                      rows = np.zeros(size_ilk))
                self.declare_partials('AEP_i', ['pdf_ilk', 'power_ilk'], method='exact',
                                      cols = np.arange(size_ilk),
                                      rows = np.arange(self.options['n_wt']).repeat(self.options['n_wd']*self.options['n_ws']))
 

    def compute(self, inputs, outputs):
        power_ilk = inputs['power_ilk']
        pdf_ilk = inputs['pdf_ilk']
        AEP_GWh = (power_ilk * pdf_ilk).sum(axis=None) * self.kWtoGWh
        AEP_GWh_i = (power_ilk * pdf_ilk).sum((1, 2)) * self.kWtoGWh
        if self.options['objective']:
            outputs['cost'] = AEP_GWh * self.cost_factor
            outputs['Cost'] = AEP_GWh
        else:  # self.options['objective'] == False
            outputs['AEP'] = AEP_GWh
            outputs['AEP_i'] = AEP_GWh_i

    def compute_partials(self, inputs, partials):
        if self.options['objective']:
            partials['cost', 'pdf_ilk'] = inputs['power_ilk'].ravel() * self.kWtoGWh * self.cost_factor
            partials['cost', 'power_ilk'] = inputs['pdf_ilk'].ravel() * self.kWtoGWh * self.cost_factor
            partials['Cost', 'pdf_ilk'] = inputs['power_ilk'].ravel() * self.kWtoGWh
            partials['Cost', 'power_ilk'] = inputs['pdf_ilk'].ravel() * self.kWtoGWh
        else:  # self.options['objective'] == False
            partials['AEP', 'pdf_ilk'] = inputs['power_ilk'].ravel() * self.kWtoGWh
            partials['AEP', 'power_ilk'] = inputs['pdf_ilk'].ravel() * self.kWtoGWh
            partials['AEP_i', 'pdf_ilk'] = inputs['power_ilk'].ravel() * self.kWtoGWh
            partials['AEP_i', 'power_ilk'] = inputs['pdf_ilk'].ravel() * self.kWtoGWh



class LoadComp(om.ExplicitComponent):
    def initialize(self):
        self.options.declare('n_wt', types=int, desc='number of wind turbines')
        self.options.declare('n_wd', types=int, desc='number of wind direction cases')
        self.options.declare('n_ws', types=int, desc='number of wind speed cases')
        self.options.declare('n_types', types=int, desc='number of different types of wind turbines')
        self.options.declare('models', types=dict, desc='dictionary of all load surrogates for all turbine types')
        self.options.declare('shear', types=(tuple, float, str, int), default=0,
                             desc='tuple of wind shear type and value i.e. ("ambient", [alpha]) or ("PyWake", ), alpha as float, or method ("PyWake" or "ambient") as string')
        self.options.declare('air_den', types=float, desc='air density')
        self.options.declare('tein', types=float, desc='terrain inclination')
        self.options.declare('wdir_unwaked', types=float, desc='wind direction for unwaked conditions')
        self.options.declare('wdir_max', types=float, desc='maximum wind direction for wake effects')
        self.options.declare('wt_dist_max', types=float, desc='maximum turbines distance in surrogates')
        self.options.declare('load_signals', types=list, desc='list of load signal names')
        self.options.declare('turb_types', types=list, desc='list of turbine types names')
        self.options.declare(topfarm.type_key, default=np.array(None), types=np.ndarray, desc='turbine type as an option')
        self.options.declare('skip_partials', default=False, types=bool, desc='if this component is part of a group where approx totals is utilized or gradient free optimization is used then the partials should not be declared')
        self.options.declare('load_compute_method', default='DWM', types=str, desc='method to compute the loads')
        self.options.declare('m_all', default={}, types=dict, desc='dictionary of Wohler exponent for each load channel')
        self.options.declare('cut_in_out', default=np.array(None), types=np.ndarray, desc='numpy array composed of multiple lists for each turbine cut in/out i.e. np.array([3,20],[4,25],[5,25])')
        self.options.declare('idling_loads', default={}, types=dict, desc='dict composed of multiple np.arrays that contain each turbine and each channels idling loads below and above cut in/out i.e. {"ch1":np.array([6500,3000],[8000,9200],[7100,8200]), "ch2":np.array([1500,2000],[1000,1200],[2100,2200])}')
        self.options.declare('MannL', default=29.4)
        self.options.declare('MannGamma', default=3.9)
        self.options.declare('Veer', default=0)
        self.options.declare('Yaw', default=0)

    def setup(self):
#        assert self.options['cut_in_out'].shape!=(), "!Error! cut_in_out can not be none, LoadComp"                    
#        assert len(self.options['idling_loads'])!=0, "!Error! idling_loads can not be empty, LoadComp"
        shear = self.options['shear']
        if isinstance(shear, tuple):
            self.shear_method, self.alpha = shear
        elif isinstance(shear, (float, int)):
            self.shear_method, self.alpha = ('ambient', shear)
        elif isinstance(shear, str):
            self.shear_method, self.alpha = (shear, np.nan)
        else:
            raise ValueError('shear should be a tuple of shear method and alpha, a float of alpha or a string of shear method')
        if self.options['load_compute_method'] == 'DWM':
            self._setup_DWM()
        elif self.options['load_compute_method'] == 'Frandsen':
            self._setup_Frandsen()
        else:
            raise ValueError('load_compute_method can only be DWM or Frandsen.')

    def compute(self, inputs, outputs):
        if self.options['load_compute_method'] == 'DWM':
            self._compute_DWM(inputs, outputs)
        elif self.options['load_compute_method'] == 'Frandsen':
            self._compute_Frandsen(inputs, outputs)
        else:
            raise ValueError('load_compute_method can only be DWM or Frandsen.')
            
    def compute_partials(self, inputs, partials):
        if self.options['load_compute_method'] == 'DWM':
            self._compute_partials_DWM(inputs, partials)
        elif self.options['load_compute_method'] == 'Frandsen':
            self._compute_partials_Frandsen(inputs, partials)
        else:
            raise ValueError('load_compute_method can only be DWM or Frandsen.')

    def _setup_DWM(self):
        self.suffix = '_DEL_iilk'
        self.shape_il = (self.options['n_wt'],
                          self.options['n_wd'])
        self.shape_ilk = (self.options['n_wt'],
                          self.options['n_wd'],
                          self.options['n_ws'])
        self.shape_iilk = (self.options['n_wt'],
                           self.options['n_wt'],
                          self.options['n_wd'],
                          self.options['n_ws'])
        self.size_iilk = np.prod(self.shape_iilk)
        self.size_ilk = np.prod(self.shape_ilk)
        self.size_il = np.prod(self.shape_il)
        if self.shear_method == 'PyWake':
            self.add_input('wind_shear_il', shape=self.shape_il)
        elif self.shear_method == 'ambient':
            self.shear_il = np.broadcast_to(self.alpha, self.shape_il)
        else:
            raise ValueError('shear type can only be PyWake or ambient.')
        self.air_den_iilk = np.broadcast_to(self.options['air_den'], self.shape_iilk)
#        self.air_den_ilk = np.broadcast_to(self.options['air_den'], self.shape_ilk)
        self.tein_iilk = np.broadcast_to(self.options['tein'], self.shape_iilk)
#        self.tein_ilk = np.broadcast_to(self.options['tein'], self.shape_ilk)
#        self.wt_dist_max_ilk = np.broadcast_to(self.options['wt_dist_max'], self.shape_ilk)
#        self.wdir_unwaked_ilk = np.broadcast_to(self.options['wdir_unwaked'], self.shape_ilk)
        self.add_input('WS_eff_ilk', shape=self.shape_ilk)
        self.add_input('TI_eff_ilk', shape=self.shape_ilk)
        self.add_input('wdir_iilk', shape=self.shape_iilk)
        self.add_input('wt_dist_iilk', shape=self.shape_iilk)
        self.MannL_iilk = np.broadcast_to(self.options['MannL'], self.shape_iilk)
        self.MannGamma_iilk = np.broadcast_to(self.options['MannGamma'], self.shape_iilk)
        self.Veer_iilk = np.broadcast_to(self.options['Veer'], self.shape_iilk)
        self.Yaw_iilk = np.broadcast_to(self.options['Yaw'], self.shape_iilk)
        if self.options[topfarm.type_key].shape==(): # add turbine type as an input if it is not an option
            self.add_input(topfarm.type_key, shape=self.options['n_wt'])
        for ls in self.options['load_signals']:
            output_name = ls + self.suffix
            self.add_output(output_name, shape=self.shape_iilk)
            if not self.options['skip_partials']:
                self.declare_partials(output_name, ['wdir_iilk', 'wt_dist_iilk'],
                                      method='exact',
                                      rows=np.arange(self.size_iilk),
                                      cols=np.arange(self.size_iilk))
                self.declare_partials(output_name, ['WS_eff_ilk', 'TI_eff_ilk'],
                                      method='exact',
                                      rows=np.arange(self.size_iilk),
                                      cols=np.tile(np.arange(self.size_ilk), self.options['n_wt']))
                if self.shear_method == 'PyWake':
                    self.declare_partials(output_name, 'wind_shear_il', method='exact',
                                          rows=np.arange(self.size_iilk),
                                          cols=np.tile(np.repeat(np.arange(self.size_il), self.options['n_ws']), self.options['n_wt']))

    def _setup_Frandsen(self):
        self.suffix = '_DEL_ik'
        self.m_list = list(set(self.options['m_all'].values()))
        n_m = len(self.m_list)
        self.shape_ik = (self.options['n_wt'],
                          self.options['n_ws'])
        self.shape_ikm = (self.options['n_wt'],
                          self.options['n_ws'],
                         n_m)
        self.size_ik = np.prod(self.shape_ik)
        self.size_ikm = np.prod(self.shape_ikm)
        if self.shear_method == 'PyWake':
            self.add_input('wind_shear_ik', shape=self.shape_ik)
        elif self.shear_method == 'ambient':
            self.shear_ik = np.broadcast_to(self.alpha, self.shape_ik)
        else:
            raise ValueError('shear type can only be PyWake or ambient.')
#        self.air_den_iilk = np.broadcast_to(self.options['air_den'], self.shape_iilk)
        self.air_den_ik = np.broadcast_to(self.options['air_den'], self.shape_ik)
#        self.tein_iilk = np.broadcast_to(self.options['tein'], self.shape_iilk)
        self.tein_ik = np.broadcast_to(self.options['tein'], self.shape_ik)
#        self.wt_dist_max_ik = np.broadcast_to(self.options['wt_dist_max'], self.shape_ik)
#        self.wdir_unwaked_ik = np.broadcast_to(self.options['wdir_unwaked'], self.shape_ik)
        self.add_input('WS_eff_ik', shape=self.shape_ik)
        self.add_input('TI_eff_ikm', shape=self.shape_ikm)
#        self.add_input('wdir_iilk', shape=self.shape_iilk)
#        self.add_input('wt_dist_iilk', shape=self.shape_iilk)
        self.MannL_ik = np.broadcast_to(self.options['MannL'], self.shape_ik)
        self.MannGamma_ik = np.broadcast_to(self.options['MannGamma'], self.shape_ik)
        self.Veer_ik = np.broadcast_to(self.options['Veer'], self.shape_ik)
        self.Yaw_ik = np.broadcast_to(self.options['Yaw'], self.shape_ik)
        if self.options[topfarm.type_key].shape==(): # add turbine type as an input if it is not an option
            self.add_input(topfarm.type_key, shape=self.options['n_wt'])
        for ls in self.options['load_signals']:
            m = self.options['m_all'][ls]
            m_index = self.m_list.index(m)
            output_name = ls + self.suffix
            self.add_output(output_name, shape=self.shape_ik)
            if not self.options['skip_partials']:
#                self.declare_partials(output_name, ['wdir_iilk', 'wt_dist_iilk'],
#                                      method='exact',
#                                      rows=np.arange(self.size_iilk),
#                                      cols=np.arange(self.size_iilk))
                self.declare_partials(output_name, ['WS_eff_ik'],
                                      method='exact',
                                      rows=np.arange(self.size_ik),
                                      cols=np.arange(self.size_ik))
                self.declare_partials(output_name, ['TI_eff_ikm'],
                                      method='exact',
                                      rows=np.arange(self.size_ik),
                                      cols=np.arange(m_index, self.size_ikm, n_m))
                if self.shear_method == 'PyWake':
                    self.declare_partials(output_name, 'wind_shear_ik', method='exact',
                                          rows=np.arange(self.size_ik),
                                          cols=np.arange(self.size_ik))

    def _compute_DWM(self, inputs, outputs):
        WS_eff_iilk = np.broadcast_to(inputs['WS_eff_ilk'][:, np.newaxis, :, :], self.shape_iilk) # this expansion will look up the wind speed for the upstream turbine
        TI_eff_iilk = np.broadcast_to(inputs['TI_eff_ilk'][np.newaxis, :, :, :], self.shape_iilk)
        if self.shear_method == 'PyWake':
            self.shear_il = inputs['wind_shear_il'].reshape(self.shape_il)
        shear_iilk = np.broadcast_to(self.shear_il[np.newaxis,:,:,np.newaxis], self.shape_iilk)
        wdir_iilk = inputs['wdir_iilk'].reshape(self.shape_iilk)
        wt_dist_iilk = inputs['wt_dist_iilk'].reshape(self.shape_iilk)
        unwaked_iilk = np.logical_or(wdir_iilk <= -self.options['wdir_max'],
                                     wdir_iilk >= +self.options['wdir_max'],
                                     wt_dist_iilk >= self.options['wt_dist_max'])
        wdir_iilk[unwaked_iilk] = self.options['wdir_max']
        wt_dist_iilk[unwaked_iilk] = self.options['wt_dist_max']
        types = np.round(np.asarray(_as_input_or_option(inputs, self.options, topfarm.type_key))).astype(np.int)
        for ls in self.options['load_signals']:
            output_name = ls + self.suffix
            DEL_iilk = np.zeros(self.shape_iilk)
            for targetcou, targetname in enumerate(self.options['turb_types']):
                for upstreamcou, upstreamname in enumerate(self.options['turb_types']):
                    type_index = np.full(self.shape_iilk, True)
                    type_index[:, types != targetcou, :, :] = False  # TODO: find a oneliner for this
                    type_index[types != upstreamcou, :, :, :] = False  # TODO: find a oneliner for this
                    wind2loads = self.options['models'][ls][upstreamname][targetname]
                    if type_index.any():
                        dummy, extrapolation_sample_waked = predict_output(
                                model=wind2loads.model,
                                input={
                                    'tint': TI_eff_iilk[type_index],
                                    'shear': shear_iilk[type_index],
                                    'wsp': WS_eff_iilk[type_index],
                                    'air_den': self.air_den_iilk[type_index],
                                    'wt_dist': wt_dist_iilk[type_index],
                                    'tein': self.tein_iilk[type_index],
                                    'wdir': wdir_iilk[type_index],
                                    'MannL': self.MannL_iilk[type_index].ravel(),
                                    'MannGamma': self.MannGamma_iilk[type_index].ravel(),
                                    'Veer': self.Veer_iilk[type_index].ravel(),
                                    'Yaw': self.Yaw_iilk[type_index].ravel(),
                                    },
                                model_in_keys=wind2loads.input_channel_names,
                                input_scaler=wind2loads.input_scaler,
                                output_scaler=wind2loads.output_scaler)
                        DEL_iilk[type_index] = dummy.ravel()
            # enforce idling loads for below/above cut in/out
            if self.options['cut_in_out'].shape != () and len(self.options['idling_loads']) != 0:
                idx = WS_eff_iilk < self.options['cut_in_out'][types][:, 0][np.newaxis, :, np.newaxis, np.newaxis]
                DEL_iilk[idx] = np.broadcast_to(self.options['idling_loads'][ls][types][:, 0][np.newaxis, :, np.newaxis, np.newaxis], self.shape_iilk)[idx]
                idx = WS_eff_iilk > self.options['cut_in_out'][types][:, 1][np.newaxis, :, np.newaxis, np.newaxis]
                DEL_iilk[idx] = np.broadcast_to(self.options['idling_loads'][ls][types][:, 1][np.newaxis, :, np.newaxis, np.newaxis], self.shape_iilk)[idx]
            
            outputs[output_name] = DEL_iilk

    def _compute_Frandsen(self, inputs, outputs):
        wt_dist_ik = np.broadcast_to(self.options['wt_dist_max'], self.shape_ik)
        wdir_ik = np.broadcast_to(self.options['wdir_max'], self.shape_ik)        
        if self.shear_method == 'PyWake':
            self.shear_ik = inputs['wind_shear_ik'].reshape(self.shape_ik)
        types = np.round(np.asarray(_as_input_or_option(inputs, self.options, topfarm.type_key))).astype(np.int)
        for ls in self.options['load_signals']:
            output_name = ls + self.suffix
            DEL_ik = np.zeros(self.shape_ik)
            if ls not in self.options['m_all']:
                raise ValueError('Using Frandsen you have to Wöler exponents for all load signals in the load comp')
            m_index = self.m_list.index(self.options['m_all'][ls])
            for cou, turbine in enumerate(self.options['turb_types']):
                type_index = np.full(self.shape_ik, True)
                type_index[types != cou, :] = False
                wind2loads = self.options['models'][ls][turbine]
                if type_index.any():
                    dummy, extrapolation_sample_waked = predict_output(
                            model=wind2loads.model,
                            input={
                                'tint': inputs['TI_eff_ikm'][:, :, m_index][type_index],
                                'shear': self.shear_ik[type_index],
                                'wsp': inputs['WS_eff_ik'][type_index],
                                'air_den': self.air_den_ik[type_index],
                                'wt_dist': wt_dist_ik[type_index],
                                'tein': self.tein_ik[type_index],
                                'wdir': wdir_ik[type_index],
                                'MannL': self.MannL_ik[type_index].ravel(),
                                'MannGamma': self.MannGamma_ik[type_index].ravel(),
                                'Veer': self.Veer_ik[type_index].ravel(),
                                'Yaw': self.Yaw_ik[type_index].ravel(),
                                },
                            model_in_keys=wind2loads.input_channel_names,
                            input_scaler=wind2loads.input_scaler,
                            output_scaler=wind2loads.output_scaler)
                    DEL_ik[type_index] = dummy.ravel()
            # enforce idling loads for below/above cut in/out
            if self.options['cut_in_out'].shape != () and len(self.options['idling_loads']) != 0:
                idx = inputs['WS_eff_ik'] < self.options['cut_in_out'][types][:, 0][:, np.newaxis]
                DEL_ik[idx] = np.broadcast_to(self.options['idling_loads'][ls][types][:, 0][:, np.newaxis], self.shape_ik)[idx]
                idx = inputs['WS_eff_ik'] > self.options['cut_in_out'][types][:, 1][:, np.newaxis]
                DEL_ik[idx] = np.broadcast_to(self.options['idling_loads'][ls][types][:, 1][:, np.newaxis], self.shape_ik)[idx]
                    
            outputs[output_name] = DEL_ik

    def _compute_partials_DWM(self, inputs, partials):
        WS_eff_iilk = np.broadcast_to(inputs['WS_eff_ilk'][:, np.newaxis, :, :], self.shape_iilk) # this expansion will look up the wind speed for the upstream turbine
        TI_eff_iilk = np.broadcast_to(inputs['TI_eff_ilk'][np.newaxis, :, :, :], self.shape_iilk)
        if self.shear_method == 'PyWake':
            self.shear_il = inputs['wind_shear_il'].reshape(self.shape_il)
        shear_iilk = np.broadcast_to(self.shear_il[np.newaxis,:,:,np.newaxis], self.shape_iilk)
        wdir_iilk = inputs['wdir_iilk'].reshape(self.shape_iilk)
        wt_dist_iilk = inputs['wt_dist_iilk'].reshape(self.shape_iilk)
        unwaked_iilk = np.logical_or(wdir_iilk <= -self.options['wdir_max'],
                                     wdir_iilk >= +self.options['wdir_max'],
                                     wt_dist_iilk >= self.options['wt_dist_max'])
        wdir_iilk[unwaked_iilk] = self.options['wdir_max']
        wt_dist_iilk[unwaked_iilk] = self.options['wt_dist_max']
        types = np.round(np.asarray(_as_input_or_option(inputs, self.options, topfarm.type_key))).astype(np.int)
        for ls in self.options['load_signals']:
            wind2loads = self.options['models'][ls][self.options['turb_types'][0]][self.options['turb_types'][0]]
            Dloads = {channel: np.zeros(self.shape_iilk) for channel in wind2loads.input_channel_names}
            output_name = ls + self.suffix
            for targetcou, targetname in enumerate(self.options['turb_types']):
                for upstreamcou, upstreamname in enumerate(self.options['turb_types']):
                    type_index = np.full(self.shape_iilk, True)
                    type_index[:, types != targetcou, :, :] = False  # TODO: find a oneliner for this
                    type_index[types != upstreamcou, :, :, :] = False  # TODO: find a oneliner for this
                    wind2loads = self.options['models'][ls][upstreamname][targetname]
                    if type_index.any():
                        dummy, extrapolation_sample_waked = predict_gradient(
                                model=wind2loads.model,
                                input={
                                    'tint': TI_eff_iilk[type_index],
                                    'shear': shear_iilk[type_index],
                                    'wsp': WS_eff_iilk[type_index],
                                    'air_den': self.air_den_iilk[type_index],
                                    'wt_dist': wt_dist_iilk[type_index],
                                    'tein': self.tein_iilk[type_index],
                                    'wdir': wdir_iilk[type_index],
                                    'MannL': self.MannL_iilk[type_index].ravel(),
                                    'MannGamma': self.MannGamma_iilk[type_index].ravel(),
                                    'Veer': self.Veer_iilk[type_index].ravel(),
                                    'Yaw': self.Yaw_iilk[type_index].ravel(),
                                    },
                                model_in_keys=wind2loads.input_channel_names,
                                input_scaler=wind2loads.input_scaler,
                                output_scaler=wind2loads.output_scaler)
                        for n, channel in enumerate(wind2loads.input_channel_names):
                            Dloads[channel][type_index] = dummy[:, n].ravel()
            # enforce zeros for partial derivatives for below/above cut in/out
            if self.options['cut_in_out'].shape != () and len(self.options['idling_loads']) != 0:
                idx = np.logical_or(WS_eff_iilk < self.options['cut_in_out'][types][:, 0][np.newaxis, :, np.newaxis, np.newaxis],
                                    WS_eff_iilk > self.options['cut_in_out'][types][:, 1][np.newaxis, :, np.newaxis, np.newaxis])
                for vals in Dloads.values():
                    vals[idx] = 0.0

            partials[output_name, 'WS_eff_ilk'] = Dloads['wsp'].ravel()
            partials[output_name, 'TI_eff_ilk'] = Dloads['tint'].ravel()
            partials[output_name, 'wdir_iilk'] = Dloads['wdir'].ravel()
            partials[output_name, 'wt_dist_iilk'] = Dloads['wt_dist'].ravel()
            if self.shear_method == 'PyWake':
                partials[output_name, 'shear_il'] = Dloads['shear'].ravel()

    def _compute_partials_Frandsen(self, inputs, partials):
        wt_dist_ik = np.broadcast_to(self.options['wt_dist_max'], self.shape_ik)
        wdir_ik = np.broadcast_to(self.options['wdir_max'], self.shape_ik)        
        if self.shear_method == 'PyWake':
            self.shear_ik = inputs['wind_shear_ik'].reshape(self.shape_ik)
        types = np.round(np.asarray(_as_input_or_option(inputs, self.options, topfarm.type_key))).astype(np.int)
        for ls in self.options['load_signals']:
            m_index = self.m_list.index(self.options['m_all'][ls])
            wind2loads = self.options['models'][ls][self.options['turb_types'][0]]
            Dloads = {}
            for channel in wind2loads.input_channel_names:
                Dloads[channel] = np.zeros(self.shape_ik)
            output_name = ls + self.suffix
            for cou, turbine in enumerate(self.options['turb_types']):
                type_index = np.full(self.shape_ik, True)
                type_index[types != cou, :] = False
                wind2loads = self.options['models'][ls][turbine]
                if type_index.any():
                    dummy, extrapolation_sample_waked = predict_gradient(
                            model=wind2loads.model,
                            input={
                                'tint': inputs['TI_eff_ikm'][:,:,m_index][type_index],
                                'shear': self.shear_ik[type_index],
                                'wsp': inputs['WS_eff_ik'][type_index],
                                'air_den': self.air_den_ik[type_index],
                                'wt_dist': wt_dist_ik[type_index],
                                'tein': self.tein_ik[type_index],
                                'wdir': wdir_ik[type_index],
                                'MannL': self.MannL_ik[type_index].ravel(),
                                'MannGamma': self.MannGamma_ik[type_index].ravel(),
                                'Veer': self.Veer_ik[type_index].ravel(),
                                'Yaw': self.Yaw_ik[type_index].ravel(),
                                },
                            model_in_keys=wind2loads.input_channel_names,
                            input_scaler=wind2loads.input_scaler,
                            output_scaler=wind2loads.output_scaler)
                    for n, channel in enumerate(wind2loads.input_channel_names):
                        Dloads[channel][type_index] = dummy[:, n].ravel()
            # enforce zeros for partial derivatives for below/above cut in/out
            if self.options['cut_in_out'].shape != () and len(self.options['idling_loads']) != 0:
                idx = np.logical_or(inputs['WS_eff_ik'] < self.options['cut_in_out'][types][:, 0][:, np.newaxis],
                                    inputs['WS_eff_ik'] > self.options['cut_in_out'][types][:, 1][:, np.newaxis])
                for vals in Dloads.values():
                    vals[idx] = 0.0
            partials[output_name, 'WS_eff_ik'] = Dloads['wsp'].ravel()
            partials[output_name, 'TI_eff_ikm'] = Dloads['tint'].ravel()
            if self.shear_method == 'PyWake':
                partials[output_name, 'shear_ik'] = Dloads['shear'].ravel()


class SoftMaxComp(om.ExplicitComponent):
    """
    Component that computes the smooth maximum of DEL_iilk.
    """

    def initialize(self):
        self.options.declare('n_wt', types=int, desc='number of wind turbines')
        self.options.declare('n_wd', types=int, desc='number of wind direction cases')
        self.options.declare('n_ws', types=int, desc='number of wind speed cases')
        self.options.declare('load_signals', types=list, desc='list of load signal names')
        self.options.declare('skip_partials', default=False, types=bool, desc='if this component is part of a group where approx totals is utilized or gradient free optimization is used then the partials should not be declared')

    def setup(self):
        self.suffix = '_DEL_ilk'
        self.shape_ilk = (self.options['n_wt'],
                          self.options['n_wd'],
                          self.options['n_ws'])
        self.shape_iilk = (self.options['n_wt'],) + self.shape_ilk
        self.size_ilk = np.prod(self.shape_ilk)
        self.size_iilk = np.prod(self.shape_iilk)
        for ls in self.options['load_signals']:
            input_name = ls + '_DEL_iilk'
            output_name = ls + self.suffix
            self.add_input(input_name, shape=self.shape_iilk)
            self.add_output(output_name, shape=self.shape_ilk)
            if not self.options['skip_partials']:
                self.declare_partials(output_name, input_name, method='exact',
                                      cols = np.arange(self.size_iilk),
                                      rows = np.tile(np.arange(self.size_ilk), self.options['n_wt']))

    def _soft_max(self, DEL_iilk):
#        return np.exp(DEL_iilk) / np.sum(np.exp(DEL_iilk), axis=0)
        return DEL_iilk / np.sum(DEL_iilk, axis=0)

    def _smooth_max(self, DEL_iilk, alpha=None):
        if not alpha:
            alpha = DEL_iilk.sum()
        return np.sum(DEL_iilk * np.exp(alpha * DEL_iilk), axis=0)/(np.sum(np.exp(alpha * DEL_iilk), axis=0))

    def _smooth_max_gradient(self, DEL_iilk, alpha=None):
        if not alpha:
            alpha = DEL_iilk.sum()
        return np.exp(alpha * DEL_iilk) / np.sum(np.exp(alpha * DEL_iilk), axis=0) * (1 + alpha * (DEL_iilk - self._smooth_max(DEL_iilk, alpha)))

    def compute(self, inputs, outputs):
        for ls in self.options['load_signals']:
            input_name = ls + '_DEL_iilk'
            output_name = ls + self.suffix
            DEL_iilk = inputs[input_name]
            soft_max = self._soft_max(DEL_iilk)
#            DEL_ilk = self._smooth_max(DEL_iilk)
            DEL_ilk = np.sum(soft_max * DEL_iilk, axis=0)
            outputs[output_name] = DEL_ilk

    def compute_partials(self, inputs, partials):
        for ls in self.options['load_signals']:
            input_name = ls + '_DEL_iilk'
            output_name = ls + self.suffix
            DEL_iilk = inputs[input_name]
            soft_max = self._soft_max(DEL_iilk)
#            smooth_max_gradient = self._smooth_max_gradient(DEL_iilk)
#            DDEL_ilk = np.zeros(self.size_ilk, self.size_iilk)
#            for i in range(n_wt):
#                DDEL_ilk[i*n_wt:(i+1)*n_wt,]
#            DDEL_ilk = np.sum(soft_max, axis=0)
#            DDEL_ilk = soft_max
            partials[output_name, input_name] = soft_max.ravel()
#            partials[output_name, input_name] = smooth_max_gradient.ravel()


class LifetimeLoadComp(om.ExplicitComponent):
    """
    Component that receives the Damage Equivalent Load from all turbine, and
    computes the Lifetime DEL.
    """
    def initialize(self):
        self.options.declare('n_wt', types=int, desc='number of wind turbines')
        self.options.declare('n_wd', types=int, desc='number of wind direction cases')
        self.options.declare('n_ws', types=int, desc='number of wind speed cases')
        self.options.declare('lifetime_on_f1zh', types=float)
        self.options.declare('m_all', types=dict,
                             desc='dictionary of Wohler exponent for each load channel')
        self.options.declare('max_load', types=dict,
                             desc='Dictionary of maximum loads for each load channel and turbine type.')
        self.options.declare('load_signals', types=list, desc='list of load signal names')
        self.options.declare(topfarm.type_key, default=np.array(None), types=np.ndarray, desc='turbine type as an option')
        self.options.declare('skip_partials', default=False, types=bool, desc='if this component is part of a group where approx totals is utilized or gradient free optimization is used then the partials should not be declared')
        self.options.declare('load_compute_method', default='DWM', types=str, desc='method to compute the loads')


    def setup(self):
        if self.options['load_compute_method'] == 'DWM':
            self._setup_DWM()
        elif self.options['load_compute_method'] == 'Frandsen':
            self._setup_Frandsen()
        else:
            raise ValueError('load_compute_method can only be DWM or Frandsen.')

    def compute(self, inputs, outputs):
        if self.options['load_compute_method'] == 'DWM':
            self._compute_DWM(inputs, outputs)
        elif self.options['load_compute_method'] == 'Frandsen':
            self._compute_Frandsen(inputs, outputs)
        else:
            raise ValueError('load_compute_method can only be DWM or Frandsen.')
            
    def compute_partials(self, inputs, partials):
        if self.options['load_compute_method'] == 'DWM':
            self._compute_partials_DWM(inputs, partials)
        elif self.options['load_compute_method'] == 'Frandsen':
            self._compute_partials_Frandsen(inputs, partials)
        else:
            raise ValueError('load_compute_method can only be DWM or Frandsen.')

    def _setup_DWM(self):
        self.shape_ilk = (self.options['n_wt'],
                          self.options['n_wd'],
                          self.options['n_ws'])
        self.shape_i = (self.options['n_wt'])
        self.size_ilk = np.prod(self.shape_ilk)
        # Invert the Wohler exponents.
        self._1_on_m_all = dict(self.options['m_all'])
        for ls, m in self.options['m_all'].items():
            self._1_on_m_all[ls] = 1 / m
        # Input and output.
        if self.options[topfarm.type_key].shape==(): # add turbine type as an input if it is not an option
            self.add_input(topfarm.type_key, shape=self.options['n_wt'])
        self.add_input('pdf_ilk', shape=self.shape_ilk)
        for ls in self.options['load_signals']:
            input_name = ls + '_DEL_ilk'
            output_name = ls# + self.suffix
            self.add_input(input_name, shape=self.shape_ilk,
                           desc='Damage Equivalent Load for ' + input_name)
            self.add_output(output_name, shape=self.shape_i,
                            desc='Normalized Lifetime DEL for ' + output_name)
            self.add_output(output_name + '_abs', shape=self.shape_i,
                            desc='Lifetime DEL for ' + output_name)
            self.add_output('post_penalty_' + ls, val=0.0)
            if not self.options['skip_partials']:
                self.declare_partials(output_name, [input_name, 'pdf_ilk'], method='exact',
                                      cols = np.arange(self.size_ilk),
                                      rows = np.arange(self.options['n_wt']).repeat(self.options['n_wd']*self.options['n_ws']))

    def _setup_Frandsen(self):
        self.shape_ik = (self.options['n_wt'],
                          self.options['n_ws'])
        self.shape_i = (self.options['n_wt'])
        self.size_ik = np.prod(self.shape_ik)
        # Invert the Wohler exponents.
        self._1_on_m_all = dict(self.options['m_all'])
        for ls, m in self.options['m_all'].items():
            self._1_on_m_all[ls] = 1 / m
        # Input and output.
        if self.options[topfarm.type_key].shape==(): # add turbine type as an input if it is not an option
            self.add_input(topfarm.type_key, shape=self.options['n_wt'])
        self.add_input('pdf_ik', shape=self.shape_ik)
        for ls in self.options['load_signals']:
            input_name = ls + '_DEL_ik'
            output_name = ls# + self.suffix
            self.add_input(input_name, shape=self.shape_ik,
                           desc='Damage Equivalent Load for ' + input_name)
            self.add_output(output_name, shape=self.shape_i,
                            desc='Normalized Lifetime DEL for ' + output_name)
            self.add_output(output_name + '_abs', shape=self.shape_i,
                            desc='Lifetime DEL for ' + output_name)
            self.add_output('post_penalty_' + ls, val=0.0)
            if not self.options['skip_partials']:
                self.declare_partials(output_name, [input_name, 'pdf_ik'], method='exact',
                                      cols = np.arange(self.size_ik),
                                      rows = np.arange(self.options['n_wt']).repeat(self.options['n_ws']))

    def _compute_DWM(self, inputs, outputs):
        pdf_ilk = inputs['pdf_ilk']
        types = np.round(np.asarray(_as_input_or_option(inputs, self.options, topfarm.type_key))).astype(np.int)
        for ls in self.options['load_signals']:
            m = self.options['m_all'][ls]
            _1_on_m = self._1_on_m_all[ls]
            DEL_ilk = inputs[ls + '_DEL_ilk']
            LDEL = ((pdf_ilk * DEL_ilk ** m).sum((1, 2)) * self.options['lifetime_on_f1zh']) ** _1_on_m
            load_normalized = LDEL / self.options['max_load'][ls][types]
            load_diff = self.options['max_load'][ls][types] - LDEL
            outputs[ls] = load_normalized
            outputs[ls + '_abs'] = LDEL
            outputs['post_penalty_' + ls] = -np.minimum(load_diff, 0).sum()


    def _compute_Frandsen(self, inputs, outputs):
        pdf_ik = inputs['pdf_ik']
        types = np.round(np.asarray(_as_input_or_option(inputs, self.options, topfarm.type_key))).astype(np.int)
        for ls in self.options['load_signals']:
            m = self.options['m_all'][ls]
            _1_on_m = self._1_on_m_all[ls]
            DEL_ik = inputs[ls + '_DEL_ik']
            LDEL = ((pdf_ik * DEL_ik ** m).sum((1)) * self.options['lifetime_on_f1zh']) ** _1_on_m
            load_normalized = LDEL / self.options['max_load'][ls][types]
            load_diff = self.options['max_load'][ls][types] - LDEL
            outputs[ls] = load_normalized
            outputs[ls + '_abs'] = LDEL
            outputs['post_penalty_' + ls] = -np.minimum(load_diff, 0).sum()

    def _compute_partials_DWM(self, inputs, partials):
        types = np.round(np.asarray(_as_input_or_option(inputs, self.options, topfarm.type_key))).astype(np.int)
        pdf_ilk = inputs['pdf_ilk']
        for ls in self.options['load_signals']:
            m = self.options['m_all'][ls]
            _1_on_m = self._1_on_m_all[ls]
            DEL_ilk = inputs[ls + '_DEL_ilk']
            max_load_i = self.options['max_load'][ls][types]
            outer_diff = 1 / m * ((pdf_ilk * DEL_ilk ** m).sum((1, 2)) * self.options['lifetime_on_f1zh']) ** (_1_on_m - 1) / max_load_i
            outer_diff = np.broadcast_to(outer_diff[:, np.newaxis, np.newaxis], self.shape_ilk)
            DLDEL_Dpdf =  outer_diff * self.options['lifetime_on_f1zh'] * DEL_ilk ** m
            DLDEL_DDEL_ilk = outer_diff * self.options['lifetime_on_f1zh'] * pdf_ilk * m * DEL_ilk ** (m - 1)
            partials[ls, ls + '_DEL_ilk'] = DLDEL_DDEL_ilk.ravel()
            partials[ls, 'pdf_ilk'] = DLDEL_Dpdf.ravel()

    def _compute_partials_Frandsen(self, inputs, partials):
        types = np.round(np.asarray(_as_input_or_option(inputs, self.options, topfarm.type_key))).astype(np.int)
        pdf_ik = inputs['pdf_ik']
        for ls in self.options['load_signals']:
            m = self.options['m_all'][ls]
            _1_on_m = self._1_on_m_all[ls]
            DEL_ik = inputs[ls + '_DEL_ik']
            max_load_i = self.options['max_load'][ls][types]
            outer_diff = 1 / m * ((pdf_ik * DEL_ik ** m).sum((1)) * self.options['lifetime_on_f1zh']) ** (_1_on_m - 1) / max_load_i
            outer_diff = np.broadcast_to(outer_diff[:, np.newaxis], self.shape_ik)
            DLDEL_Dpdf =  outer_diff * self.options['lifetime_on_f1zh'] * DEL_ik ** m
            DLDEL_DDEL_ik = outer_diff * self.options['lifetime_on_f1zh'] * pdf_ik * m * DEL_ik ** (m - 1)
            partials[ls, ls + '_DEL_ik'] = DLDEL_DDEL_ik.ravel()
            partials[ls, 'pdf_ik'] = DLDEL_Dpdf.ravel()
