# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
setup(name='workshop', 
      description='Topfarm - Workshop files',
      url='hhttps://gitlab.windenergy.dtu.dk/TOPFARM/workshop-december-2019',
      author='DTU Wind Energy',  
      author_email='mikf@dtu.dk',
      license='MIT',
      packages=find_packages(),
      zip_safe=True)
